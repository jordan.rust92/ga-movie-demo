const webpack = require('webpack');
module.exports = {
    devtool: 'source-map',
    entry: './src/index',
    mode: 'development',
    target : 'web'
};
