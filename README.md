## How to run:
- To run this app, call these two commands:
- npm run build
- npm run start

- To develop, call these two commands in separate terminals:
- npm run watch
- npm run start:dev