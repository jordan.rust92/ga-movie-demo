// Now here's something interesting and very powerful. By using imports, you can split your js
// into multiple files. The only problem is, the browser can't read this. You'll need a tool like webpack
// to convert your files into something the browser can understand. I won't go into webpack now, but it
// is a very commonly used tool that makes your frontend code much more modular. That said, node understands
// imports, so use it all you want in your backend code!
// That said, this imports 3 functions from our pageManager.js file.
import { closeDetail, populateSearchResultsContainer, showAndPopulateDetailContainer } from './pageManager';

// So we're declaring two variables here, one with const and one with let. So what's the difference?
// Well const is short for Constant. This means once we set the value of the searchInput variable, 
// it can NEVER change.
// Now let variables like previousSearch, those can change all we want. If you look down in handleSearch,
// you'll see we reassign it down there.
const searchInput = document.getElementById('input-search');
let previousSearch = '';

const addToFavorites = (id) => {
    // Most frameworks give you easy ways to hit backend services but there's actually a few easy ways to
    // do this with vanilla js. Some people use XHR requests, but we want to use a promise structure (because its fun)
    // so we're using fetch. Fetch defaults to GETs, but we're doing a POST here. Remember, these need to match your
    // backend for this to work.
    fetch('/favorites', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json; charset=utf-8'
        },
        body: JSON.stringify({OMDBID: id})
    }).then(() => closeDetail());
}

// Ok so I didn't quite finish the favorites, but this function does work. I'd probably call it on load of the favorites page
// and then populate the page using a genericized version of populateSearchResultsContainer and then use the same templates
// as search results except with a remove from favorites button. Searching and showing favorites are almost the same so making
// generic functions in pageManager would make that pretty clean and concise.
const getFavorites = () => fetch(`/favorites`).then((res) => res.json());

const handleSearch = (event) => {
    const isEnter = event.code === 'Enter'; // Find keycodes at keycode.info (use the event.code. It is much more readable)
    const newSearch = event.code ? searchInput.value : '';
    if(isEnter && newSearch !== previousSearch) { 
        previousSearch = newSearch;
        fetch(`/search/${newSearch}`)
            .then((res) => res.json())
            .then((searchResults) => {
                populateSearchResultsContainer(searchResults.Search);
            });
    }
}

// We're adding a listener here. Listeners allow you to react to what the user is doing on the page. Here we call the handleSearch
// function anytime someone presses down a key within the searchInput element.
searchInput.addEventListener('keydown', handleSearch);

// Ok so why are we attaching things to the window? Well, webpack minifys your code and would make sure that your functions
// aren't available to your html. This is a good thing, because we don't want everything to be public. Problem is, we do want a 
// few things to be public. By attaching them to the window, our onclick listeners in our html have access to them.
window.addToFavorites = addToFavorites;
window.closeDetail = closeDetail;
window.showAndPopulateDetailContainer = showAndPopulateDetailContainer;