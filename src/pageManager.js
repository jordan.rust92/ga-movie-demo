const searchResultsContainer = document.getElementById('search-results-container');
const pageWrap = document.getElementsByClassName('page-wrap')[0];

const addElementToPage = (el, target) => target.appendChild(el);

const clearSearchResults = () => {
    // While loops perform whatever is in the curly brackets as long as their condition resolves to true
    // Be careful though that they don't go on forever. I know this will eventually end, because searchResultsContainer
    // can't have infinite elements. 
    while(searchResultsContainer.firstChild) {
        searchResultsContainer.removeChild(searchResultsContainer.firstChild);
    }
}

const closeDetail = event => {
    const detailEl = event ? event.target : document.getElementsByClassName('detail-wrap')[0];
    detailEl.parentElement.removeChild(detailEl)
};

// We're using templates to build out common repeatable html. Now in react you would do something similar
// to this, and in angular, you'd just have ngModels and binding in your html. But in vanilla js we're gonna
// use templates. We're using our helper, htmlToElement to take our strings and turn them into actual html elements.
// Then they are ready to stick in the dom
const createDetailTemplateEl = (data) => {
    return htmlToElement(`
        <div class="detail-wrap" onclick="closeDetail(event)">
            <div class="detail" onclick="event.stopPropagation()">
                <img src="${data.Poster}" alt="">
                <div>
                    <div>Title: ${data.Title}</div>
                    <div>Year: ${data.Year}</div>
                    <div>Rating: ${data.Rated}</div>
                    <div>${data.Plot}</div>

                    <button onclick="addToFavorites('${data.imdbID}')">Add to Favorites</button>
                </div>
            </div>
        </div>
    `);
}

// I didn't mention before, but what is this weird ${data.Poster} thing I'm doing below?? That is called
// string interpolation. That's just a complicated way of saying, I'm adding javascript into my strings!
// What this means, is that anything inside of a ${} will be evaluated and then added to the overall string.
const createMovieTemplateEl = (data) => {
    return htmlToElement(`
        <div class="search-item" onclick="showAndPopulateDetailContainer('${data.imdbID}')">
            <img src="${data.Poster}" alt="">
            <div>
                <div>Title: ${data.Title}</div>
                <div>Year: ${data.Year}</div>
            </div>
        </div>
    `);
}

// Arrow functions automatically return whatever is in them if you don't write them with a return statement
// and they're written as one line. So this actually returns the promise from fetch to whoever calls this.
// That promise then resolves to the res.json() result.
const getDetailById = id => fetch(`/detail/${id}`).then((res) => res.json());

const showAndPopulateDetailContainer = async (id) => {
    // await is another way of dealing with promises. Instead of having a promise.then() structure, we're telling
    // our js to wait until that promise is resolved, then set the resolved value to our variable. Much shorter,
    // but notice we aren't doing any type of error handling here. That is dangerous, and in a case like this,
    // you'd want to wrap it in a try-catch.
    const details = await getDetailById(id);
    addElementToPage(createDetailTemplateEl(details), pageWrap);
}

const populateSearchResultsContainer = (results) => {
    clearSearchResults();
    // forEach is just a loop. We're saying, go through the results list, call every element in that list a movie,
    // then do what is in the curly brackets. There are lots of looping functions in js for various needs. Try
    // checking out filter, find, and map. You'll use those alot!
    results.forEach((movie) => {
        addElementToPage(createMovieTemplateEl(movie), searchResultsContainer);
    });
}

// Full disclosure, this htmlToElement is stolen from stack overflow. One of your most important skills as a developer
// is to know how and what to search for when you are stuck on a problem. I didn't know specifically how to concisely make a
// template element in vanilla js, so I looked it up. You NEVER want to copy something you found online without knowing
// what it does though. Unless it is an email regex. Those are dumb.
const htmlToElement = (html) => {
    const template = document.createElement('template');
    html = html.trim();
    template.innerHTML = html;
    return template.content.firstChild;
}

// Remember when we imported stuff in the index.js? This is where we decide what is available to import from
// this file. That means everything else in here is private. 
export { closeDetail, populateSearchResultsContainer, showAndPopulateDetailContainer };