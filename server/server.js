const express = require('express');
const app = express();
const request = require('request-promise-native');
const bodyParser = require('body-parser');
const fs = require('fs');

// We are pulling in our key from an environment variable. This is very important as you don't
// want to be commiting your api keys to a public git repository. That would be like taping
// your keys side of your car door when you went shopping. 
const OMDB_KEY = process.env.OMDB_KEY;

app.use(express.static('dist')); // By telling express to 'use' the dist folder, we're making all of its contents public to our users
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json()); // Bodyparser allows us to look at the data we're being sent in requests

// We are doing a few cool things in this function. You'll see we're using an arrow function to build
// this function. This allows us to be a bit more concise since we're really just creating a string.
// You might wonder why we're passing
// in a key instead of just grabbing the OMDB_KEY. By passing in the parameter, we make this much more
// functional, meaning it doesn't rely on the state of the application. This means if you call the function
// multiple times with the same parameters, you ALWAYS get the same result. Testing is much easier this way.
// We also encode the url, making sure any special characters get translated to something that work in a url.
const buildOMDBUrl = (type, searchTerms, key) => encodeURI(`http://www.omdbapi.com/?${type}=${searchTerms}&apikey=${key}`);

// We've pulled this logic out of the search and id routes below, since we're doing the exact same thing in both of them.
// This uses the request library to make a GET request to OMDB. This is very similar to the fetch that we're using on the 
// front end of the application. Remember, this is asynchronous, so we're using then and catch to deal with our results.
const getOMDBData = (res, type, searchTerms, key) => {
  request.get(buildOMDBUrl(type, searchTerms, key))
      .then(body => res.send(body))
      .catch(err => res.send(err));
}

// Here we're declaring a GET endpoint that will look like this: movieApp.com/search/starwars.
// The :searchTerms tells express to look for that part of the url and capture it under req.params.
app.get('/search/:searchTerms', (req, res) => {
  // This is called a ternary. It is basically a shortened version of an if-else statement. They look like this
  // condition ? trueAction : falseAction
  // So in our case, if we have searchTerms, we're going to go get the OMDB data. If we don't, we'll return a 404
  req.params.searchTerms ? getOMDBData(res, 's', req.params.searchTerms, OMDB_KEY) : res.status(404).end();
});

app.get('/detail/:id', (req, res) => {
  req.params.id ? getOMDBData(res, 'i', req.params.id, OMDB_KEY) : res.status(404).end();
});

// We want to use a get here because we are getting information from the server
app.get('/favorites', (req, res) => {
  const data = fs.readFileSync('./data.json'); // Because we are using the SYNC version of readFile, we don't need to use callbacks here
  res.setHeader('Content-Type', 'application/json');
  res.send(data);
});

// Post is used for sending data to the backend to modify or create objects.
// We'll take the OMDBID that we are sent and push it into our array that holds our favorites
app.post('/favorites', (req, res) => {
  if(!req.body || !req.body.OMDBID){ // If we don't have a body or an ID in the body, we would want to error
    res.send('Error');
  } else {
    const data = JSON.parse(fs.readFileSync('./data.json'));
    data.push(req.body.OMDBID);
    // writeFile is asynchronous and asks for a callback function. A callback is just a function
    // that will be called once writeFile is finished doing what it does.
    fs.writeFile('./data.json', JSON.stringify(data), () => {
      res.status(200).send(data);
    });
  }
});

app.listen(3000, () => {
  if(!OMDB_KEY) {
    // In addition to the log function, console also has functions such as info or error
    // You can use these to give your messages different levels of importance. In this case,
    // our app won't work without the OMDB_KEY so we'd want to use error.
    console.error('Failed to load OMDB_KEY. We will fail all requests to them.');
  }
  console.log('Listening on port 3000');
});